package com.renseki.app.projectpertamaku

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_my_output.*

class myOutputActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_output)

        val myBundle = intent.extras
        val myNama = myBundle.getString("nama")
        myOutputView.text = "Welcome, $myNama"

    }
}
